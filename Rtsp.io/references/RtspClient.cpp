/******************************************************************************/
/** @file
 *
 * @brief	Implementation for the RTSP client session object
 *
 *		Implements the RTSP client protocol.  See RFC-2326.  Also refers to
 *		RFC-2068 (the HTTP spec) and RFC-4555 (the SDP standard).
 *
 * @author
 *		Michael K. Jones\n
 *		Stone Hill Consulting\n
 *		http://www.stonehill.com\n
 *
 * @par Environment
 *		Windows Embedded Standard 7
 *		Microsoft Visual Studio 2010
 *		Qt Framework 4.8.3
 *****************************************************************************/
#include "RtspClient.h"
#include "Events.h"
#include <QStringList>
#include <QHostAddress>
#include <QSettings>
#include <QPair>


// Constants for building up RTPS requests, and parsing responses
const char SP[]						= " ";
const char CRLF[]					= "\r\n";
const char RTSP_VER[]				= "RTSP/1.0";
const uint RTSP_PORT				= 554;

// Header fields
const char HDR_ACCEPT[]				= "Accept:";
const char HDR_CONTENT_LENGTH[]		= "Content-Length:";
const char HDR_CONTENT_TYPE[]		= "Content-Type:";
const char HDR_CSEQ[]				= "CSeq:";
const char HDR_METHOD_CODE[]		= "Method-Code:";
const char HDR_SESSION[]			= "Session:";
const char HDR_TRANSPORT[]			= "Transport:";
const char HDR_USER_AGENT[]			= "User-Agent:";
const char HDR_RANGE[]				= "Range:";
const char HDR_SCALE[]				= "Scale:";
const char HDR_SERVER[]				= "Server:";
const char HDR_TIANSHAN_APPDATA[]		= "TianShan-AppData:";
const char HDR_TIANSHAN_CLIENT_TIMEOUT[]= "TianShan-ClientTimeout:";
const char HDR_TIANSHAN_NOTICE[]		= "TianShan-Notice:";
const char HDR_TIANSHAN_NOTICEPARAM[]	= "TianShan-NoticeParam:";
const char HDR_TIANSHAN_SERVICEGROUP[]	= "TianShan-ServiceGroup:";
const char HDR_TIANSHAN_TRANSPORT[]		= "TianShan-Transport:";
const char HDR_TIANSHAN_VERSION[]		= "TianShan-Version:";


// Various values
const char USER_AGENT[]				= "Enseo HD-2000";	// We have to lie to the server
const char APPLICATION_SDP[]		= "application/sdp";
const char TEXT_PARAMETER[]			= "text/parameter";
const char TIANSHAN_VERSION[]		= "1.0";
const char POSITIONSCALE[]			= "Position Scale";

// Method names for RTSP
const char GET_PARAMETER[]			= "GET_PARAMETER";
const char OPTIONS[]				= "OPTIONS";
const char DESCRIBE[]				= "DESCRIBE";
const char SETUP[]					= "SETUP";
const char TEARDOWN[]				= "TEARDOWN";
const char PLAY[]					= "PLAY";
const char PAUSE[]					= "PAUSE";
const char ANNOUNCE[]				= "ANNOUNCE";

// Parameters
const char TIMEOUT[]				= "timeout";
const uint SESSION_TIMEOUT_DEFAULT	= 60;	// Seconds
const char SESSION_ALREADY_SETUP[]	= "session already setuped";
const char NPT[]					= "npt";
const char CLOCK[]					= "clock";


// Helper to convert the headers into a string (for printing)
QString RtspClient::ToString(const RtspHeaders &h)
{
	QString s = "";
	RtspHeaders::const_iterator i = h.constBegin();
	while (i != h.constEnd())
	{
		s += QString("%1 %2\r\n").arg(i.key()).arg(i.value());
		++i;
	}
	return s;
}


/******************************************************************************/
/**
 * Constructor; initialize a new instance of the object
 *
 * @return
 *		None
 *
 ******************************************************************************/
RtspClient::RtspClient(QObject *parent)
	: QObject	(parent)	// Parent of this object
	, _socket	(this)		// Socket to use for communicating with server
	, _cseq		(1)			// Sequence number for requests
	, _received	()			// Init receive buffer to empty
	, _session	()			// Session ID (init to empty)
	, _timeout	(SESSION_TIMEOUT_DEFAULT)
	, _sessionKeepAliveTimer(this)
{
	// Connect up signals from the socket; most socket operations are done
	// asynchronously, so the results only come back via a signal.
	connect(&_socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(onSocketError(QAbstractSocket::SocketError)) );
	connect(&_socket, SIGNAL(connected()),	SLOT(onSocketConnected()) );
	connect(&_socket, SIGNAL(disconnected()),SLOT(onSocketDisconnected()) );
	connect(&_socket, SIGNAL(readyRead()),	SLOT(onSocketReadyRead()) );

	// Connect the session keep-alive timer (used after we open a session
	// to send pings to the server)
	connect(&_sessionKeepAliveTimer, SIGNAL(timeout()), SLOT(onSessionKeepAlive()));

	return;
}


/******************************************************************************/
/**
 * Destructor; clean up the object as it is going away
 *
 * @return
 *		None
 *
 ******************************************************************************/
RtspClient::~RtspClient()
{
	logger()->debug("Destroying %1", _url.toString());
	Stop();
	return;
}

bool RtspClient::Stop()
{
	bool result = true;
	logger()->debug("Stopping client");
	if(IsConnected())
	{
		Teardown();
		Disconnect();
	}
	return result;
}


/******************************************************************************/
/**
 * Set the RTSP URL to access.
 *
 * @return
 *		True if the URL was set; false if not (e.g. connection is currently open)
 *
 ******************************************************************************/
bool RtspClient::SetUrl(QString url)
{
	bool result = false;

	// If socket is open, fail (caller must close previous session first)
	if (_socket.state() != QAbstractSocket::UnconnectedState)
	{
		logger()->error("SetUrl(%1) failed; previous connection still open", url);
	}
	else
	{
		// Set the URL
		logger()->trace("SetUrl(%1)", url);
		_url = url;

		// If no scheme was specified, default to RTSP
		if (_url.scheme().isEmpty())
			_url.setScheme("rtsp");

		// If no port was specified, default to 554
		if (_url.port() < 0)
			_url.setPort(RTSP_PORT);

		// If URL is invalid, return an error
		result = _url.isValid();
	}
	return result;
}


/******************************************************************************/
/**
 * Initiate a connection to the server
 *
 * @return
 *		True if connection was successfully initiated.  False if not (could
 *		be already connected, or invalid URL)
 *
 ******************************************************************************/
bool RtspClient::Connect()
{
	bool result = false;

	// If socket is open, fail (caller must close previous session first)
	if (_socket.state() != QAbstractSocket::UnconnectedState)
	{
		logger()->error("Connect() failed; previous connection still open");
	}
	else
	{
		// Try to initiate a connection to the specified server
		logger()->trace("Connect to host %1:%2", _url.host(), QString::number(_url.port()));
		_socket.connectToHost(_url.host(), _url.port());
		result = true;
	}
	return result;
}


/******************************************************************************/
/**
 * Close the connection to the server
 *
 * @return
 *		True if disconnect was successfully initiated.  False if not (i.e. not
 *		currently connected)
 *
 ******************************************************************************/
bool RtspClient::Disconnect()
{
	bool result = false;

	// If socket is open, initiate the disconnect
	if (_socket.state() != QAbstractSocket::UnconnectedState)
	{
		// Try to initiate disconnect
		logger()->trace("Disconnect from host %1:%2", _url.host(), QString::number(_url.port()));
		_socket.disconnectFromHost();
		result = true;
	}
	else
	{
		logger()->error("Disconnect() failed; connection is not open");
	}
	return result;
}

/******************************************************************************/
/**
 * Check if the connection to the server is ok
 *
 * @return
 *		True if connected to server
 *
 ******************************************************************************/
bool RtspClient::IsConnected()
{
	return (_socket.state() == QAbstractSocket::ConnectedState);
}

/******************************************************************************/
/**
 * Initiate a SETUP to create a session for the specified URL.
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
bool RtspClient::Setup()
{
	bool result = false;

	logger()->trace("Initiate SETUP");

	// Error if no connection, or if there's an active session
	if (_socket.state() != QAbstractSocket::ConnectedState)
	{
		logger()->error("Setup failed; connection is not open");
	}
	if (!_session.isEmpty())
	{
		logger()->error("Setup failed; session already active (id=%1)", _session);
	}
	else
	{
		RtspHeaders hdrs;
#if 0
		// Include what version of the server we're expecting
		hdrs[HDR_TIANSHAN_VERSION] = TIANSHAN_VERSION;
#endif

		// Process all of the query fields in the URL (certain query items go to
		// specific fields in the header)
		QPair<QString,QString> pair;
		foreach (pair, _url.queryItems())
		{
			// The transport has it's own header, and also requires additional
			// data.
			if (pair.first.compare("transport", Qt::CaseInsensitive) == 0)
			{
				// Fetch the transport from the URL
				QString transport = pair.second;

				// We assume unicast (as opposed to multicast)
				transport += ";unicast";

				// If transport is over IP, we need additional fields
				if (transport.contains("UDP", Qt::CaseInsensitive))
				{
					// Use the IP address we're connected to the server on
					QHostAddress ip = _socket.localAddress();
					transport += QString(";destination=%1").arg(ip.toString());

					// Use the UDP port from the settings file, but supply a default
					// value in case it's not present.
					QSettings settings;
					uint port = settings.value("VideoUdpPort", 1235).toUInt();
					transport += QString(";client_port=%1").arg(port);
				}

				// Save the header value
				hdrs[HDR_TRANSPORT] = transport;
			}

			// ServiceGroup has its own header line
			else if (pair.first.compare("ServiceGroup", Qt::CaseInsensitive) == 0)
			{
#if 0			// For now, don't include the ServiceGroup header
				hdrs[HDR_TIANSHAN_SERVICEGROUP] = _url.queryItemValue("ServiceGroup");
#endif
			}

			// Everything else gets dumped into the AppData header
			else
			{
#if 0			// For now, don't include the AppData header
				if (!hdrs[HDR_TIANSHAN_APPDATA].isEmpty())
					hdrs[HDR_TIANSHAN_APPDATA] += ';';
				hdrs[HDR_TIANSHAN_APPDATA] += QString("%1=%2").arg(pair.first).arg(pair.second);
#endif
			}
		}

		// Fire away!
		result = SendRequest(SETUP, hdrs);
	}
	return result;
}


/******************************************************************************/
/**
 * Initiate a TEAROWN of the session
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
bool RtspClient::Teardown()
{
	bool result = false;

	logger()->trace("Initiate TEARDOWN");

	// Error if no connection, or no active session
	if (_socket.state() != QAbstractSocket::ConnectedState)
	{
		logger()->error("Teardown failed; connection is not open");
	}
	if (_session.isEmpty())
	{
		logger()->error("Teardown failed; no active session");
	}
	else
	{
		RtspHeaders hdrs;

		result = SendRequest(TEARDOWN, hdrs);
	}
	return result;
}


/******************************************************************************/
/**
 * Initiate a PLAY request
 *
 * @return
 *		True if the request was sent; false if unable to send
 *
 ******************************************************************************/
bool RtspClient::Play(float speed, QString range)
{
	bool result = false;

	logger()->trace("Initiate PLAY(%1)", speed);

	if (_socket.state() != QAbstractSocket::ConnectedState)
	{
		logger()->error("Play failed; connection is not open");
	}
	else if (_session.isEmpty())
	{
		logger()->error("Play failed; no active session");
	}
	else
	{
		RtspHeaders hdrs;

#if 0
		// Include what version of the server we're expecting
		hdrs[HDR_TIANSHAN_VERSION] = TIANSHAN_VERSION;
#endif

		// RFC-2326 says the Range header is optional, and if not present, the
		// server should play starting from 0 (or resume from a previous PAUSE).
		// Sending a PLAY while the stream is playing is essentially a NOP (spec
		// says client can do that to test server aliveness).

		// RtspSpecForEnseo.pdf says Range can be absolute:
		//		Range: clock=UTC[-UTC]
		// Where UTC=YYYYMMDDTHHMMSS[.frame]; or relative:
		//		Range: npt=SEC[-SEC]
		// Where SEC is time offset from beginning, in seconds; or
		//		Range: now
		// Where "now" will lead to a position that is nearest the current clock time.
		// Empty values will be ignored without repositioning, i.e.
		//		Range:
		//		Range: npt=
		//		Range: clock=
		// Server will respond with 457 Invalid Range is it doesn't like the range we
		// specify.
		
		//hdrs[HDR_RANGE] = QString("%1=%2-").arg(NPT).arg(0);
		hdrs[HDR_RANGE] = range;

		// Scale is a floating point value.  For example, 1 means normal forward
		// viewing rate. -1 means normal rate but backwards.  2 would mean twice the
		// normal rate forward, etc.  The decimal part is optional if it is 0
		hdrs[HDR_SCALE] = QString("%1").arg(speed, 0, 'f');

		// Fire away!
		result = SendRequest(PLAY, hdrs);
	}
	return result;
}


/******************************************************************************/
/**
 * Initiate a PAUSE request
 *
 * @return
 *		True if the request was sent; false if unable to send
 *
 ******************************************************************************/
bool RtspClient::Pause()
{
	bool result = false;

	logger()->trace("Initiate PAUSE");

	if (_socket.state() != QAbstractSocket::ConnectedState)
	{
		logger()->error("Pause failed; connection is not open");
	}
	else if (_session.isEmpty())
	{
		logger()->error("Pause failed; no active session");
	}
	else
	{
		RtspHeaders hdrs;

		// There are not additional headers to supply, apart from what SendRequest
		// will add automatically (CSeq, Session, and User-Agent).

		// Fire away!
		result = SendRequest(PAUSE, hdrs);
	}
	return result;
}

/******************************************************************************/
/**
 * Initiate a GET_PARAMETER request and get the NPT back
 *
 * @return
 *		Npt in seconds
 *
 ******************************************************************************/
bool RtspClient::GetPosition()
{
	bool result = false;
	logger()->trace("Initiate GetPosition");

	if (_socket.state() != QAbstractSocket::ConnectedState)
	{
		logger()->error("GetPosition failed; connection is not open");
	}
	else if (_session.isEmpty())
	{
		logger()->error("GetPosition failed; no active session");
	}
	else
	{
		RtspHeaders hdrs;

		// Fire away!
		result = GetParameter(POSITIONSCALE);
	}
	return result;
}


/******************************************************************************/
/**
 * Slot called whenever an error occurs on the socket
 *
 * @return
 *		None
 *
 ******************************************************************************/
void RtspClient::onSocketError(QAbstractSocket::SocketError err)
{
	logger()->error("*** %1", _socket.errorString());

	// Signal that a socket error has occurred
	emit SocketError(err, _socket.errorString());
	return;
}


/******************************************************************************/
/**
 * Slot called when the socket is connected to the host.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void RtspClient::onSocketConnected()
{
	logger()->trace("Connected to %1:%2", _url.host(), QString::number(_url.port()));

	// Signal that we have connected
	emit Connected();
	return;
}


/******************************************************************************/
/**
 * Slot called when the socket is disconnected from the host.
 *
 * @return
 *		None
 *
 ******************************************************************************/
void RtspClient::onSocketDisconnected()
{
	logger()->trace("Disconnected from %1:%2", _url.host(), QString::number(_url.port()));
	_sessionKeepAliveTimer.stop();
	_session.clear();

	// Signal that we have disconnected
	emit Disconnected();
	return;
}


/******************************************************************************/
/**
 * Slot called when new data is available to read
 *
 * @return
 *		None
 *
 ******************************************************************************/
void RtspClient::onSocketReadyRead(void)
{
	// Read whatever data is available, and append it to our receive buffer
	logger()->debug("onSocketReadyRead, %1 bytes available", _socket.bytesAvailable());
	_received += _socket.readAll();

	// If we have two consecutive CRLF's, then we have a complete response.
	// Process it!  Actually, it's possible there is more than one response
	// in the buffer, so process all of them
	QString endOfResponse = CRLF; endOfResponse += CRLF;
	int index;
	while (0 <= (index = _received.indexOf(endOfResponse)))
	{
		// Include the double CRLF's in the response header
		index += endOfResponse.length();
		QString response = _received.left(index);

		// See if this is a response
		int code=0;
		QString phrase="";
		QString method="";
		QString url="";
		RtspHeaders hdrs;
		if (parseResponse(response, code, phrase, hdrs))
		{
			// If there's body content, see if we've got enough data in
			// the receive buffer
			int contentLength = 0;
			if (hdrs.contains(HDR_CONTENT_LENGTH))
				contentLength = hdrs[HDR_CONTENT_LENGTH].toInt();
			if (contentLength > _received.length() - index)
			{
				// This response has a content body, but we haven't received
				// all of it yet.  Just quit now; when more data is received,
				// we will process the response then.
				break;
			}

			// Remove the response from the receive buffer
			_received.remove(0, index);

			// Now extract the content body
			QString body = _received.left(contentLength);
			_received.remove(0, contentLength);

			// Handle the response
			ProcessResponse (code, phrase, hdrs, body);
		}

		// If not a response, see if it's a request the server is sending to us
		else if (parseRequest(response, method, url, hdrs))
		{
			logger()->debug("Request: %1 %2\n%3", method, url, ToString(hdrs));

			// Remove the response from the receive buffer
			_received.remove(0, index);

			// Handle the response
			if(method == ANNOUNCE) {
				ProcessAnnounce(hdrs);
			}
		}

		// Don't know WHAT it is!
		else
		{
			logger()->warn("Unrecognized message recieved (ignoring):\n{\n%1\n}", response);

			// Remove the response from the receive buffer
			_received.remove(0, index);
		}
	}
	logger()->debug("onSocketReadyRead, %1 bytes left in receive buffer", _received.length());
}


/******************************************************************************/
/**
 * Called when the session keep-alive timer goes off
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
void RtspClient::onSessionKeepAlive(void)
{
	// If no session, or socket not open, kill the timer
	if (_session.isEmpty() || (_socket.state() != QAbstractSocket::ConnectedState))
		_sessionKeepAliveTimer.stop();

	// Otherwise, send a "ping", which is just a GET_PARAMETER with no body.
	else
		GetParameter();
}


/******************************************************************************/
/**
 * Send a GET_PARAMETER request to the server
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
bool RtspClient::GetParameter(QString parm)
{
	logger()->trace("Send %1(%2)", GET_PARAMETER, parm);
	RtspHeaders hdrs;

	// Specify the content type
	hdrs[HDR_CONTENT_TYPE] = TEXT_PARAMETER;

	return SendRequest(GET_PARAMETER, hdrs, parm);
}


/******************************************************************************/
/**
 * Send an OPTIONS request to the server
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
bool RtspClient::SendOptions()
{
	logger()->trace("Send %1", OPTIONS);
	const RtspHeaders hdrs;
	return SendRequest(OPTIONS, hdrs);
}


/******************************************************************************/
/**
 * Send a DESCRIBE request to the server
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
bool RtspClient::SendDescribe()
{
	logger()->trace("Send %1", DESCRIBE);
	RtspHeaders hdrs;

	// The DESCRIBE command needs to specify the type of content we accept in
	// the response.  The usual 
	hdrs[HDR_ACCEPT] = APPLICATION_SDP;

	return SendRequest(DESCRIBE, hdrs);
}


/******************************************************************************/
/**
 * Send a request to the server
 *
 * @return
 *		True if request was sent; false if not
 *
 ******************************************************************************/
bool RtspClient::SendRequest(
	const QString method,		///< Request method
	const RtspHeaders &headers,	///< Optional headers to add to request
	const QString body)			///< Optional content body to send after headers
{
	bool result = false;

	if (_socket.state() == QAbstractSocket::ConnectedState)
	{
		// Start line contains the requested method, URL, and RTSP version
		QString req = QString("%1 %2 %3\r\n").arg(method).arg(_url.toString()).arg(RTSP_VER);

		// Put the current sequence number into the header.
		// Note: we only leave one request outstanding at a time; we don't
		// increment the sequence number until the response is received
		req += QString("%1 %2\r\n").arg(HDR_CSEQ).arg(QString::number(_cseq));

		// Put in the standard headers we add for every request
		req += QString("%1 %2\r\n").arg(HDR_USER_AGENT).arg(USER_AGENT);

		// If we have an active session, put it's ID into the header
		if (!_session.isEmpty())
			req += QString("%1 %2\r\n").arg(HDR_SESSION).arg(_session);

		// Message header line(s)
		RtspHeaders::const_iterator i = headers.constBegin();
		while (i != headers.constEnd())
		{
			req += QString("%1 %2\r\n").arg(i.key()).arg(i.value());
			++i;
		}

		// If there's a message body, we must include the content length
		if (body.length() > 0)
			req += QString("%1 %2\r\n").arg(HDR_CONTENT_LENGTH).arg(QString::number(body.length()));

		// A blank line terminates the header
		req += CRLF;

		// If there's a message body, it comes next
		if (body.length())
			req += body;

		// Send the request
		logger()->trace("Sending request:\n{\n%1}", req);
		result = _socket.write(req.toUtf8()) == req.length();
	}
	return result;
}


/******************************************************************************/
/**
 * Parse a response.  Extracts the response code and phrase (if any), and any
 * headers which are present.
 *
 * @return
 *		True if valid response; false if not
 *
 ******************************************************************************/
bool RtspClient::parseResponse(
	const QString &response,///< Response string to parse
	int &code,				///< Where to return response code
	QString &phrase,		///< Where to return response phrase
	RtspHeaders &hdrs)		///< Where to return response headers
{
	bool result = false;
	int a=-1, b=-1;
	bool ok = false;

	// Initialize the returned components of the response
	code = 0;
	phrase = "";
	hdrs.clear();

	// Split the response into separate lines.  We expect a blank line at the end
	// so we specify "SkipEmptyParts" so it doesn't get stored in the list
	QStringList fields;
	QStringList lines = response.split(CRLF, QString::SkipEmptyParts);
	if (lines.isEmpty())
	{
		logger()->debug("Response contains no lines!");
	}

	// Remove the first line from the list, and split it on spaces
	else if ((fields = lines.takeFirst().split(SP)).length() < 2)
	{
		logger()->debug("Invalid response line format");
	}

	// First field must be the RTSP version
	else if (fields[0] != RTSP_VER)
	{
		logger()->debug("Response doesn't start with %1 (got %2 instead)",
			RTSP_VER, fields[0]);
	}

	// Next comes the response code
	else if (!(code = fields[1].toInt(&ok)) || !ok)
	{
		logger()->debug("Invalid response code:\n%1", fields[1]);
	}
	else 
	{
		// The rest of the first line is the response phrase (which is optional)
		fields.removeFirst();		// Remove RTSP version
		fields.removeFirst();		// Remove response code
		phrase = fields.join(SP);	// Join rest of fields into single string

		// Any remaining response lines are header fields; parse them into the
		// map
		/// @TODO	Does not handle values continued on subsequent lines (i.e.
		//			where line begins with whitespace).
		foreach(QString line, lines)
		{
			if ((fields = line.split(':')).length() < 2)
			{
				logger()->debug("Invalid header line (no colon?): '%1'", line);
			}
			else
			{
				QString fieldName = fields.takeFirst();
				fieldName += ':';

				// The following line re-joins the value into a single field,
				// in case it contains colons (which we split, above).  It also
				// trims off leading and trailing whitespace.
				hdrs[fieldName] = fields.join(":").trimmed();
			}
		}

		// Declare success!
		result = true;
	}

	return result;
}


/******************************************************************************/
/**
 * Parse a request.  Extracts the request method and URL, and any headers which
 * are present.
 *
 * @return
 *		True if valid response; false if not
 *
 ******************************************************************************/
bool RtspClient::parseRequest(
	const QString &request,	///< Response string to parse
	QString &method,		///< Where to return request method
	QString &url,			///< Where to return URL from request
	RtspHeaders &hdrs)		///< Where to return request headers
{
	bool result = false;
	int a=-1, b=-1;
	bool ok = false;

	// Initialize the returned components of the response
	method = "";
	hdrs.clear();

	// Split the response into separate lines.  We expect a blank line at the end
	// so we specify "SkipEmptyParts" so it doesn't get stored in the list
	QStringList fields;
	QStringList lines = request.split(CRLF, QString::SkipEmptyParts);
	if (lines.isEmpty())
	{
		logger()->debug("Request contains no lines!");
	}

	// Remove the first line from the list, and split it on spaces
	else if ((fields = lines.takeFirst().split(SP)).length() != 3)
	{
		logger()->debug("Invalid request line format");
	}

	// Fields are <method> <url> RTSP/1.0
	// Check for RTSP version
	else if (fields[2] != RTSP_VER)
	{
		logger()->debug("Request doesn't end with %1", RTSP_VER);
	}

	else 
	{
		// Save the request method and URL
		method = fields[0];
		url = fields[1];

		// Any remaining response lines are header fields; parse them into the
		// map
		/// @TODO	Does not handle values continued on subsequent lines (i.e.
		//			where line begins with whitespace).
		lines.removeFirst();
		foreach(QString line, lines)
		{
			if ((fields = line.split(':')).length() < 2)
			{
				logger()->debug("Invalid request header line (no colon?): '%1'", line);
			}
			else
			{
				QString fieldName = fields.takeFirst();
				fieldName += ':';

				// The following line re-joins the value into a single field,
				// in case it contains colons (which we split, above).  It also
				// trims off leading and trailing whitespace.
				hdrs[fieldName] = fields.join(":").trimmed();
			}
		}

		// Declare success!
		result = true;
	}

	return result;
}


/******************************************************************************/
/**
 * Process a response received from the server.
 *
 * @return
 *		True for success; false for error
 *
 ******************************************************************************/
bool RtspClient::ProcessResponse (
	int code,			///< Response code
	QString phrase,		///< Response phrase
	RtspHeaders hdrs,	///< Response headers
	QString body)		///< Response body content
{
	logger()->trace("Response received: %1 %2", QString::number(code), phrase);
	logger()->trace("Response headers:\n{\n%1}", ToString(hdrs));
	logger()->debug("Response content:\n{\n%1\n}", body);

	bool result = false;

	// If the sequence number matches, this is the response to the request we
	// sent out.  Currently, we only send one request at a time.
	if (hdrs[HDR_CSEQ].toInt() == _cseq)
	{
		// Increment the sequence number
		_cseq++;

		// If the request failed, give up?
		if (code != 200)
		{
			logger()->error("Request failed; error %1 %2", QString::number(code), phrase);
		}

		// Do the next thing (based on what just completed)
		if (hdrs[HDR_METHOD_CODE] == GET_PARAMETER)
		{
			// If no body, do nothing.  This is probably the response to a ping.
			/// @TODO	Perhaps we should verify the response to a ping?
			if (!body.isEmpty())
			{
				// If body is present, build up the response to send to javascript.
				RtspResponseEvent resp(hdrs[HDR_METHOD_CODE], code, phrase);
				QStringList lines = body.trimmed().split(CRLF, QString::SkipEmptyParts);
				foreach (QString pair, lines)
				{
					QStringList param = pair.split(':');
					if (param.length() == 2)
						resp.insert(param[0], param[1].trimmed());
				}
				emit RtspResponse(resp);
			}
		}
		else if (hdrs[HDR_METHOD_CODE] == OPTIONS)
		{
			//SendDescribe();
		}
		else if (hdrs[HDR_METHOD_CODE] == DESCRIBE)
		{
			//SendSetup();
		}
		else if (hdrs[HDR_METHOD_CODE] == SETUP)
		{
			// Grab the session ID.  Note that it may have an optional timeout specified
			QStringList fields = hdrs[HDR_SESSION].split(';');
			if (fields.length() < 1)
			{
				/// @TODO	No session ID???
			}
			else
			{
				// First field is the session ID
				_session = fields.takeFirst();

				// Look through any other fields specified, and process the ones we
				// care about
				bool timeoutSpecified = false;
				foreach (QString f, fields)
				{
					QStringList param = f.split('=');
					if (param[0].compare(TIMEOUT, Qt::CaseInsensitive) == 0)
						_timeout = param[1].toUInt(&timeoutSpecified);
				}

				// Use default timeout if none specified (or invalid)
				if (!timeoutSpecified)
					_timeout = SESSION_TIMEOUT_DEFAULT;
			}

			// Special case: if the session is already open (e.g. we crashed leaving
			// it open), handle that
			if (code == 455) // "Method Not Valid in This State"
			{
				if (hdrs[HDR_TIANSHAN_NOTICE].compare(SESSION_ALREADY_SETUP, Qt::CaseInsensitive) == 0)
				{
					logger()->debug("Session %1 already setup; using previous session", _session);
					code = 200;
				}
			}

			// Build up the response to send to javascript.
			RtspResponseEvent resp(hdrs[HDR_METHOD_CODE], code, phrase);
			if (hdrs.contains(HDR_TIANSHAN_TRANSPORT))
			{
				QStringList fields = hdrs[HDR_TIANSHAN_TRANSPORT].split(';');
				if (fields.length() > 0)
				{
					// The first field is the transport used (e.g. MP2T/DVBC/QAM)
					resp.insert ("Transport", fields.takeFirst());

					// Rest of the fields should be name/value pairs; process them all
					foreach (QString pair, fields)
					{
						QStringList param = pair.split('=');
						if (param.length() == 2)
							resp.insert(param[0], param[1]);
					}
				}

				// Also process normal Transport header (e.g. MP2T/AVP/UDP)
				fields = hdrs[HDR_TRANSPORT].split(';');
				if (fields.length() > 0)
				{
					// The first field is the transport used (e.g. MP2T/DVBC/QAM)
					resp.insert ("Transport", fields.takeFirst());

					// Rest of the fields should be name/value pairs; process them all
					foreach (QString pair, fields)
					{
						QStringList param = pair.split('=');
						if (param.length() == 2)
							resp.insert(param[0], param[1]);
					}
				}
			}

			// If we have an active session, start the keep alive timer.  Fire it
			// once per minute (or half the timeout time, whichever is less)
			if ((code == 200) && !_session.isEmpty())
			{
				_sessionKeepAliveTimer.start(qMin<uint>(60000, _timeout/2 * 1000));
				emit RtspResponse(resp);
			}
			else
			{
				emit RtspResponse(resp);
			}
		}
		else if (hdrs[HDR_METHOD_CODE] == TEARDOWN)
		{
			_sessionKeepAliveTimer.stop();
			_session.clear();
			emit RtspResponse(RtspResponseEvent(hdrs[HDR_METHOD_CODE], code, phrase));
		}
		else if (hdrs[HDR_METHOD_CODE] == PLAY)
		{
			// Verify the session ID
			if (hdrs[HDR_SESSION] != _session)
				logger()->error("Invalid session in PLAY response; expected %1, got %2", _session, hdrs[HDR_SESSION]);

			// Build up the response to send to javascript.
			RtspResponseEvent resp(hdrs[HDR_METHOD_CODE], code, phrase);

			if (hdrs.contains(HDR_SCALE))
				resp.insert(HDR_SCALE, hdrs[HDR_SCALE].trimmed());

			if (hdrs.contains(HDR_RANGE))
				resp.insert(HDR_RANGE, hdrs[HDR_RANGE].trimmed());

			emit RtspResponse(resp);
		}
		else if (hdrs[HDR_METHOD_CODE] == PAUSE)
		{
			// Verify the session ID
			if (hdrs[HDR_SESSION] != _session)
				logger()->error("Invalid session in PAUSE response; expected %1, got %2", _session, hdrs[HDR_SESSION]);

			emit RtspResponse(RtspResponseEvent(hdrs[HDR_METHOD_CODE], code, phrase));
		}
	}

	// If the response sequence number doesn't match, something has gone wrong
	else
	{
		/// @TODO send an event to Javascript
		logger()->error("Response sequence number doesn't match");
		//_socket.close(); // Just ignore this message, don't close connection
	}

	return result;
}


bool RtspClient::ProcessAnnounce(RtspHeaders hdrs)
{
	bool result = true;

	QVariantMap req;

	if (hdrs.contains(HDR_TIANSHAN_NOTICE))
	{
		req.insert(HDR_TIANSHAN_NOTICE, hdrs[HDR_TIANSHAN_NOTICE]);
	}

	if (hdrs.contains(HDR_TIANSHAN_NOTICEPARAM))
	{
		QStringList fields = hdrs[HDR_TIANSHAN_NOTICEPARAM].split(';');
		if (fields.length() > 0)
		{
			// All fields should be name/value pairs; process them all
			foreach (QString pair, fields)
			{
				QStringList param = pair.split('=');
				if (param.length() == 2)
					req.insert(param[0], param[1]);
			}
		}
	}
	
	emit RtspAnnounce(req);
	return result;
}