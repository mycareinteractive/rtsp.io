# RTSP.io Proxy Server #

This package proxies an RTSP stream from SeaChange to "another source". The "other source" is dictated by the proxy's [browser client](https://bitbucket.org/aceso/rtsp.io-client-js). 

Upon initial clone, you can run:

```
npm install
```

to install dependencies. Once done, you can run:

```
node app.js
```

and this will start a basic HTTP server that is accessed by going to [http://localhost:8888](http://localhost:8888) in your browser.

Once loaded in the browser, some instructions are given how to operate the test simulator.