﻿/**
 *  A channel consists a socket.io socket and a rtsp client
 */
var RtspClient = require('./rtspclient.js');

function Channel(uid, socket, router) {
    this.router = router;
    this.uid = uid;
    this.socket = socket;
    this.client = null;
    this.device = null;
    this.room = null;
    
    this.duration = 0;
    
    this.socketHandler = {
        'load': this.load,
        'play': this.play,
        'pause': this.pause,
        'currentTime': this.currentTime,
        'teardown': this.teardown
    };
    
    this.events = {
        loaded: 'rtspio.loaded',
        error: 'rtspio.error',
        playing: 'rtspio.playing',
        paused: 'rtspio.paused',
        ended: 'rtspio.ended',
        beginning: 'rtspio.beginning',
        currentTime: 'rtspio.currentTime',
        announced: 'rtspio.announced',
        teardown: 'rtspio.teardown'
    };
}

Channel.prototype.load = function (url) {
    console.log('Channel:load');
    var self = this;
    var socket = self.socket;
    
    if (self.client) {
        console.log('Rtsp client already exists, disconnect and create a new one.');
        //self.client.disconnect();
        delete self.client;
    }
    
    self.client = new RtspClient(self.uid);
    var client = self.client;
    
    client.once('connect', function () {
        client.setup();
    });
    
    client.once('disconnect', function () {
        self.destroy();
    });
    
    client.once('setup', function (code, phrase, param) {
        if (code == "200") {
            // we are loaded
            socket.emit(self.events.loaded, phrase, param);
        } else {
            console.log('rtsp.io: We have had an error on setup.', phrase, self.info());
            socket.emit(self.events.error, code, phrase, param);
        }
    });
    
    client.on('play', function (code, phrase, param) {
        if (code == "200") {
            // ok we are playing
            socket.emit(self.events.playing, phrase, param);
        } else {
            console.log('rtsp.io: We have had an error on Play.', phrase, self.info());
            socket.emit(self.events.error, code, phrase, param);
        }
    });
    
    client.on('pause', function (code, phrase, param) {
        if (code == "200") {
            socket.emit(self.events.paused, phrase, param);
        } else {
            console.log('rtsp.io: We have had an error on setup.', phrase, self.info());
            socket.emit(self.events.error, code, phrase, param);
        }
    });
    
    client.on('get_parameter', function (code, phrase, param) {
        if (code == "200") {
            socket.emit(self.events.currentTime, phrase, param);
        } else {
            socket.emit(self.events.error, code, phrase, param);
        }
    });
    
    client.on('announce', function (notice, param) {
        if (notice) {
            if (notice.indexOf('End-of-Stream') >= 0) {
                // dispatch an event back to the player
                socket.emit(self.events.ended, notice, param);
            } else if (notice.indexOf('Beginning-of-Stream')) {
                socket.emit(self.events.beginning, notice, param);
            }
        }
        socket.emit(self.events.announced, notice, param);
    });
    
    client.on('teardown', function (code, phrase, param) {
        socket.emit(self.events.teardown, phrase, param);
    });
    
    if (client && !client.connected)
        client.connect(url);
    
};

Channel.prototype.destroy = function () {
    if (this.client) {
        this.client.disconnect();
        delete this.client;
    }
    this.client = null;
    
    if (this.socket) {
        this.socket.disconnect(true);
    }
    this.socket = null;
    
    if (this.router) {
        this.router.kill(this.uid);
    }
};

Channel.prototype.play = function (scale, offset) {
    var self = this;
    //self._check();
    var client = self.client;
    var range;
    
    if (offset)
        range = "npt=" + offset;

    if(!self._check()) {
        console.log('Failed in: Channel.play');
        return false;
    }

    console.info('Playing movie', this.info());
    client.play(scale, range);
};

Channel.prototype.pause = function () {
    var self = this;
    var client = self.client;

    if(!self._check()) {
        console.log('Failed in: Channel.pause');
        return false;
    }
    
    client.pause();
};

Channel.prototype.currentTime = function () {
    var self = this;
    var client = self.client;

    if(!self._check()) {
        console.log('Failed in: Channel.currentTime');
        return false;
    }
    
    client.getParameter();
};

Channel.prototype.teardown = function () {
    var self = this;
    var client = self.client;

    if(!self._check()) {
        console.log('Failed in: Channel.teardown');
        return false;
    }
    
    client.teardown();
    
    // after teardown, close the connection no matter what
    setTimeout(function () {
        self.destroy();
    }, 200);
};

// use this to pass as the meta object to cpnsole.log
// we will output identifiable channel markers
// for easier error tracking
Channel.prototype.info = function(){
    var out = "\n<<( ";
    var id = {
        ip: this.uid,
        room: this.room,
        device: this.device,
        socket: (this.socket|| {}).id
    };

    for(var i in id) {
        out += i + ": " + id[i] + " | ";
    }

    out = out.substr(0, out.length - 3) + " )>>";
    out += "\n";

    return out;

}

Channel.prototype._check = function(msg){
    var chk = true;

    if(!this.client) {
        console.log('Rtsp client not found.', this.info());
        return false;
    }

    return chk;
}

module.exports = Channel;