﻿/**
 * Encapsulating a TCP socket as Rtsp Client
*/
var net = require('net');
var events = require('events');
var Emitter = require('events').EventEmitter;
var urlParser = require('url');
var config = require('config');
var RtspMessage = require('./rtspmessage.js');


var CONST = RtspMessage.constants;
var line_regexp = /^([^\n]*\n)/;

function RtspClient(uid) {
    this.uid = uid;
    
    this.reset();
    
    var parts = uid.split(':');
    if (parts.length > 1) {
        this.destinationIp = parts[0];
        this.destinationPort = parts[1];
    }
    else {
        this.destinationIp = parts[0];
        this.destinationPort = 1235;
    }

    events.EventEmitter.call(this);
    
    // Every method will have a corresponding event as a "response"
    // The events we are going to emit
    this.events = {
        connect: 'connect',
        disconnect: 'disconnect',
        announce: 'announce',
        options: 'options',
        describe: 'describe',
        setup: 'setup',
        play: 'play',
        pause: 'pause',
        getParameter: 'getParameter',
        teardown: 'teardown',
        announce: 'announce'
    };
}

/**
 * Inherits from `EventEmitter` so we can fire up events
 */
RtspClient.prototype.__proto__ = Emitter.prototype;

RtspClient.prototype.reset = function () {
    this.url = null;
    this.session = null;
    this.seq = 1;
    this.socket = null;
    this.connected = false;
    this._received = '';
};

/**
 * Connect to the RTSP server
 */
RtspClient.prototype.connect = function (url) {
    var self = this;

    self.url = url;
    // parse url for host and port
    var parser = urlParser.parse(self.url);
    var host = parser.hostname;
    var port = parser.port || 554;
    
    // connect the socket and hookup some raw socket events
    var socket = self.socket = new net.Socket();
    
    console.info('[%s] Connecting to server %s:%d...', self.uid, host, port);
    socket.connect(port, host, function () {
        self.connected = true;
        console.info('[%s] Connected to server %s:%d', self.uid, host, port);
        // emit 'connect' event
        self.emit(self.events.connect);
    });
    
    self.socket.on('data', function (data) {
        console.log('[%s] RX:\n%s', self.uid, data);
        self._recv(data);
    });
    
    socket.on('close', function () {
        self.connected = false;
        console.log('[%s] Connection closed', self.uid);
        // emit 'close' event
        self.emit(self.events.disconnect);
    });

    socket.on('error', function () {
        self.connected = false;
        console.log('[%s] Connection error', self.uid);
        // emit 'close' event
        self.emit(self.events.disconnect);
    });
}

/**
 * Disconnect
 */ 
RtspClient.prototype.disconnect = function () {
    var self = this;

    if (self.connected) {
        self.teardown();
    }
    if (self.socket) {
        self.socket.destroy();
        self.connected = false;

    }
}

RtspClient.prototype.isConnected = function () {
    return this.connected;
}

RtspClient.prototype.options = function () {
    var self = this;
};

RtspClient.prototype.describe = function () {
    var self = this;
};

RtspClient.prototype.setup = function () {
    var self = this;

    if (!this.connected) {
        self.emit('setup', null, null, { message: 'Not connected to server.' });
        return;
    }
    
    if (this.session) {
        self.emit('setup', null, null, { message: 'One session already established.' });
        return;
    }
    
    var hdrs = {};   // all RTSP headers
    
    var request = new RtspMessage(CONST.SETUP, self.url, this);

    // Process query fields in the URL (certain query items go to Transport Header)
    var parser = urlParser.parse(self.url, true);
    var transport = parser.query['transport'] || 'MP2T/AVP/UDP';
    transport += ';unicast';
    if (transport.indexOf('UDP')) { // if it's udp stream we need to figure out the destination IP/port
        transport += ';destination=' + self.destinationIp;
        transport += ';client_port=' + self.destinationPort;
    }
    request.addHeader(CONST.HDR_TRANSPORT, transport);

    // fire
    return self._send(request);
};

RtspClient.prototype.play = function (scale, range) {
    var self = this;
    var request = new RtspMessage(CONST.PLAY, null, this);

    if (scale) {
        request.addHeader(CONST.HDR_SCALE, scale);
    }

    if (typeof range != 'undefined') {
        request.addHeader(CONST.HDR_RANGE, range);
    }

    return self._send(request);
};

RtspClient.prototype.pause = function () {
    var self = this;
    var request = new RtspMessage(CONST.PAUSE, null, this);

    return self._send(request);
};

RtspClient.prototype.teardown = function () {
    var self = this;
    var request = new RtspMessage(CONST.TEARDOWN, null, this);

    return self._send(request);
};

RtspClient.prototype.getParameter = function () {
    var self = this;
    var request = new RtspMessage(CONST.GET_PARAMETER, null, this);
    request.addHeader(CONST.HDR_CONTENT_TYPE, CONST.TEXT_PARAMETER);
    request.setBody(CONST.POSITIONSCALE);

    return self._send(request);
};

RtspClient.prototype._onResponse = function (message) {
    // handle responses and emit different events
    var self = this;
    var ev, code, phrase, param;
    
    code = message.response.code;
    phrase = message.response.phrase;
    
    var headers = message.headers;
    var cseq = parseInt(headers[CONST.HDR_CSEQ]);
    if (cseq >= self.seq) {
        this.seq = cseq + 1;
    }
    
    var method = headers[CONST.HDR_METHOD_CODE];
    if (method == CONST.SETUP && !self.session) {
        // setup response carries some extra stuff
        // session
        this.session = headers[CONST.HDR_SESSION];
        // transport header
        param = { transport: headers[CONST.HDR_TRANSPORT] };
    }
    
    if (method == CONST.GET_PARAMETER) {
        param = message.body; 
    }
    
    // emit event to channel
    if (method) {
        ev = method.toLowerCase().trim();
        console.log('[%s] response for %s', self.uid, ev);
        self.emit(ev, code, phrase, param);
    }
};

RtspClient.prototype._onAnnounce = function (message) {
    // handle announce and emit different events
    var self = this;

    var ev = 'announce';
    var notice = message.headers[CONST.HDR_TIANSHAN_NOTICE];
    var param = message.headers[CONST.HDR_TIANSHAN_NOTICEPARAM];

    self.emit(ev, notice, param);
};

RtspClient.prototype._send = function (request) {
    var self = this;

    if (!this.connected)
        return fn({ notConnected: '', message: 'Not connected to server.' });
    
    var bytes = request.toString();
    console.log('[%s] TX:\n%s', self.uid, bytes);
    
    self.socket.write(bytes);
}

RtspClient.prototype._recv = function (data) {
    // Read whatever data is available, and append it to our receive buffer
    console.log("_recv, %d bytes available", data.length);
    this._received += data;
    
    // If we have two consecutive CRLF's, then we have a complete response.
    // Process it!  Actually, it's possible there is more than one response
    // in the buffer, so process all of them
    var endOfResponse = '\r\n\r\n';
    var index;
    while (0 <= (index = this._received.indexOf(endOfResponse))) {
        // Include the double CRLF's in the response header
        index += endOfResponse.length;
        var response = this._received.substr(0, index);
        
        // See if this is a response
        var message = RtspMessage.parse(response);
        if (message && message.getType() == 'response') {
            // If there's body content, see if we've got enough data in the receive buffer
            var contentLength = 0;
            if (message.headers[CONST.HDR_CONTENT_LENGTH])
                contentLength = message.headers[CONST.HDR_CONTENT_LENGTH];
            if (contentLength > this._received.length - index) {
                // This response has a content body, but we haven't received all of it yet.  
                // Just quit now; when more data is received, we will process the response then.
                break;
            }
            
            // Remove the response from the receive buffer
            this._received = this._received.substr(index);
            
            // Now extract the content body
            var body = this._received.substr(0, contentLength);
            this._received = this._received.substr(contentLength);
            message.setBody(body);
            
            // Handle the response
            this._onResponse(message);
        }

		// If not a response, see if it's a request the server is sending to us
        else if (message && message.getType() == 'request') {
            
            // Remove the response from the receive buffer
            this._received = this._received.substr(index);
            
            // Handle the response
            if (message.request.method == CONST.ANNOUNCE) {
                this._onAnnounce(message);
            }
        }

		// Don't know WHAT it is!
        else {
            console.warn("Unrecognized message recieved (ignoring):\n{\n%s\n}", response);
            // Remove the response from the receive buffer
            this._received = this._received.substr(index);
        }
    }
    console.log("_recv, %d bytes left in receive buffer", this._received.length);
};

module.exports = RtspClient;

var dummyFn = function () { };
