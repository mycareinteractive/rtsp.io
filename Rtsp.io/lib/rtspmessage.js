﻿var util = require('util');
var urlParser = require('url');
var config = require('config');
var RtspClient = require('./rtspclient.js');

/**
 * Create a new message
 */
function RtspMessage(method, url, client) {
    this.request = {
        method : method,
        url : url || '*'
    };
    this.response = null;
    this.seq = 1;
    this.session = '';
    this.headers = {};
    this.body = null;
    
    if (client && client instanceof RtspClient.constructor) {
        this.seq = client.seq;
        this.session = client.session;
    }
}

RtspMessage.prototype.addHeader = function (key, value) {
    if (key) {
        this.headers[key] = value;
    }
};

RtspMessage.prototype.setBody = function (body) {
    this.body = body;
};

RtspMessage.prototype.getType = function () {
    return (this.response)? 'response' : 'request';
};

/**
 * Static method. Parse message as a response or request
 */
RtspMessage.parse = function (str) {
    var message = null;
    if (!str)
        return null;
    var lines = str.split('\r\n');
    
    if (lines.length == 0) {
        console.log("message contains no lines!");
        return null;
    }
    
    // Remove the first line from the list, and split it on spaces
    var fields = lines[0].split(' ');
    if (fields.length < 3) {
        console.log("Invalid message line format");
        return null;
    }
    
    message = new RtspMessage();
    message.request = message.response = null;
    if (fields[0] == CONST.RTSP_VER) {
        // Response, First field must be the RTSP version
        if (!parseInt(fields[1])) {
            console.log("Invalid message code: %s", fields[1]);
            return null;
        }
        message.response = {
            code: parseInt(fields[1]),
            phrase: fields.slice(2).join(' ')
        };
    }
    else if (fields[2] == CONST.RTSP_VER) {
        // Request
        message.request = {
            method: fields[0],
            url: fields[1]
        };
    }
    
    lines.splice(0, 1);
    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];
        if (!line)
            continue;   // blank line
        fields = line.split(':')
        if (fields.length < 2) {
            console.log("Ignore invalid header line (no colon?): '%s'", line);
            continue;
        }
        else {
            var fieldName = fields[0];
            fieldName += ':';
            
            // The following line re-joins the value into a single field,
            // in case it contains colons (which we split, above).  It also
            // trims off leading and trailing whitespace.
            message.addHeader(fieldName, fields.slice(1).join(":").trim());
        }
    }
    return message;
};


/**
 * Stringify message with everything populated properly.
 * This method takes care of headers like CSeq, User-Agent and Content-Length
 */
RtspMessage.prototype.toString = function () {
    var self = this;
    var str = '';
    //PLAY * RTSP / 1.0
    if (self.request)
        str += util.format('%s %s %s\r\n', self.request.method, self.request.url, CONST.RTSP_VER);
    else if (self.response)
        str += util.format('%s %s %s\r\n', CONST.RTSP_VER, self.response.code, self.response.phrase);
    
    // CSeq: 4
    str += util.format('%s %d\r\n', CONST.HDR_CSEQ, self.seq);
    // User-Agent: Enseo HD-2000
    str += util.format('%s %s\r\n', CONST.HDR_USER_AGENT, CONST.USER_AGENT);
    // Session: 2423204190
    if (self.session)
        str += util.format('%s %s\r\n', CONST.HDR_SESSION, self.session);
    // Other headers
    for (var name in self.headers) {
        str += util.format('%s %s\r\n', name, self.headers[name]);
    }
    // If there's a message body, we must include the content length
    if (self.body)
        str += util.format('%s %s\r\n', CONST.HDR_CONTENT_LENGTH, self.body.length);
    
    // A blank line terminates the header
    str += '\r\n';
    
    // If there's a message body, it comes next
    if (self.body)
        str += self.body;
    
    return str;
};

var CONST = RtspMessage.constants = {
    
    // Special strings
    SP : ' ',
    CRLF : '\r\n',
    RTSP_VER : 'RTSP/1.0',
    
    // Headers
    HDR_ACCEPT : 'Accept:',
    HDR_CONTENT_LENGTH : 'Content-Length:',
    HDR_CONTENT_TYPE : 'Content-Type:',
    HDR_CSEQ : 'CSeq:',
    HDR_METHOD_CODE : 'Method-Code:',
    HDR_SESSION : 'Session:',
    HDR_TRANSPORT : 'Transport:',
    HDR_USER_AGENT : 'User-Agent:',
    HDR_RANGE : 'Range:',
    HDR_SCALE : 'Scale:',
    HDR_SERVER : 'Server:',
    HDR_TIANSHAN_APPDATA : 'TianShan-AppData:',
    HDR_TIANSHAN_CLIENT_TIMEOUT : 'TianShan-ClientTimeout:',
    HDR_TIANSHAN_NOTICE : 'TianShan-Notice:',
    HDR_TIANSHAN_NOTICEPARAM : 'TianShan-NoticeParam:',
    HDR_TIANSHAN_SERVICEGROUP : 'TianShan-ServiceGroup:',
    HDR_TIANSHAN_TRANSPORT : 'TianShan-Transport:',
    HDR_TIANSHAN_VERSION : 'TianShan-Version:',
    
    // Method names for RTSP
    GET_PARAMETER : 'GET_PARAMETER',
    OPTIONS : 'OPTIONS',
    DESCRIBE : 'DESCRIBE',
    SETUP : 'SETUP',
    TEARDOWN : 'TEARDOWN',
    PLAY : 'PLAY',
    PAUSE : 'PAUSE',
    ANNOUNCE : 'ANNOUNCE',
    
    // Useful Parameters/values
    USER_AGENT : 'Enseo HD-2000',// We have to lie to the server
    APPLICATION_SDP : 'application/sdp',
    TEXT_PARAMETER : 'text/parameter',
    TIANSHAN_VERSION : '1.0',
    POSITIONSCALE : 'Position Scale',
    TIMEOUT : 'timeout',
    SESSION_TIMEOUT_DEFAULT : 60, // Seconds
    SESSION_ALREADY_SETUP : 'session already setuped',
    NPT : 'npt',
    CLOCK : 'clock'
};

module.exports = RtspMessage;


