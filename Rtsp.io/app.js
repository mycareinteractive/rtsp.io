﻿var http = require('http').createServer(handler)
  , io = require('socket.io')()
  , config = require('config')
  , debug = require('debug')
  , path = require('path')
  , fs = require('fs');

var Log = require('./log.js');
var Channel = require('./lib/channel.js');
var Router = require('./lib/router.js');


// help Date functions
Date.prototype.stdTimezoneOffset = function () {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.dst = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

// http handler
function handler(req, res) {

    if (req.url.substr(0, 5) == '/time') {
        var d = new Date();
        var objTime = {
            year: d.getFullYear(),
            month: d.getMonth() + 1,
            day: d.getDate(),
            hour: d.getHours(),
            minute: d.getMinutes(),
            second: d.getSeconds(),
            gmtOffsetInMinute: d.stdTimezoneOffset(),
            isDaylightSaving: d.dst()
        };
        var str = "window.serverTime = " + JSON.stringify(objTime) + ";";

        res.setHeader("Content-Length", str.length);
        res.setHeader('Content-Type', 'text/javascript');
        res.statusCode = 200;
        res.end(str.toString());

    } else if(req.url.substr(0, 3) == '/js') {
        var file = 'public' + req.url;
        console.log(file);
        fs.readFile(path.resolve(__dirname, file), function (err, content) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading javascript: ' + file + '\n' + err);
            }
            res.setHeader("Content-Length", content.length);
            res.setHeader('Content-Type', 'text/javascript');
            res.statusCode = 200;
            res.end(content);
        });
    } else {
        fs.readFile(path.resolve(__dirname, 'public/index.html'), function (err, content) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index-socketio.html');
            }
            res.setHeader("Content-Length", content.length);
            res.setHeader('Content-Type', 'text/html');
            res.statusCode = 200;
            res.end(content);
        });
    }
}

function initialize() {
    
    // Attach to http server and set socket.io options. (eg: transport method)
    // Change config/production.config to change it.
    if (config.has('socketio.options'))
        io.attach(http, config.get('socketio.options'));
    
    // Optional settings
    if (config.has('socketio.origins'))
        io.origins(config.get('socketio.origins'));
    
    io.set('origins', '*:*');
    
    if (process.env.IISNODE_VERSION) {
        // If this node.js application is hosted in IIS, assume it is hosted in IIS virtual directory named 'rtspio' and set up 
        // the socket.io 's path to recognize requests that target it. 
        // Note a corresponding change in the client index-socketio.html, as well as necessary URL rewrite rule in web.config. 
        //io.path(config.get('socketio.path'));
    }
    
    // Middleware goes here
    io.use(function (socket, next) {
        if (true) return next();
        next(new Error('Authentication error'));
    });
    
    var router = new Router(io);

    // Register event handlers
    io.on('connection', function (socket) {
        var socketId = socket.id;
        var clientIp = socket.request.connection.remoteAddress || socket.handshake.headers.host;
        console.log('socket %s connected from %s', socketId, clientIp);
        
        // 'handshake' should be the first message.  All following messages will be routed by Router to each channel.
        socket.on('handshake', function (uid, room, device) {
            router.handshake(socket, uid, room, device);
        });

    });


}

initialize();
http.listen(process.env.PORT || 8888);