﻿# Rtsp.io

Rtsp.io is a lightweighted application that proxy between modern WebSocket clients to legacy Rtsp servers.
With Rtsp.io, clients such as Smart TV will be able to request Rtsp streams from legacy Rtsp servers with low latency and
real-time bi-directional notifications.


