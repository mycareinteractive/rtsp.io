﻿var Channel = require('./channel.js');

function Router(io) {
    this.io = io;
    this.channels = {};
}

/**
 * A client identifies itself by a unique id.  For regular devices such as Smart TVs the id is client IP address.
 * For devices that can hold multiple streams such as a QAM Broadcast scheduler, this id is destination ip:port.
 */ 
Router.prototype.handshake = function (socket, uid, room, device) {
    var self = this;
    console.log("socket handshake for [%s], device %s, room %s", uid, device, room);
    this.socket = socket;
    this.socket.join(uid);
    
    // get the channel by ID, or create a new one
    var channel = self.channels[uid];
    if (!channel) {
        channel = new Channel(uid, socket, this);
        channel.room = room;
        channel.device = device;
        self.channels[uid] = channel;
    }
    
    // Bind all events to channel
    for (var event in channel.socketHandler) {
        socket.on(event, channel.socketHandler[event].bind(channel));
    }

    console.log('Handshake complete, sending event');
    self.socket.emit('rtspio.handshake');
    return;
};

Router.prototype.kill = function (uid) {
    // remove the channel from the list
    var channel = this.channels[uid];
    if (channel) {
        delete this.channels[uid];
        console.log('Channel [%s] removed!', uid);
    }
};

/**
 * Get called periodically to destroy orphan channels
 */
Router.prototype._monitor = function () {

};

module.exports = Router;